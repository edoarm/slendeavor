<?php

class Config_model extends CI_Model
{
    private $table = "config";

    public function getParam($name)
    {
        $query = $this->db->select('text')
                            ->from($this->table)
                            ->where('param', $name)
                            ->get()
                            ->row();
        if(!isset($query)) {
            return null;
        }

        return $query->text;
    }

    public function setParam($name, $value)
    {
        $param = $this->getParam($name);

        if($param === null) {
            $this->db->insert($this->table, array('param' => $name, 'text' => $value));
        } else {
            $this->db->where('param', $name);
            return $this->db->update($this->table, array('text' => $value));
        }

    }

}