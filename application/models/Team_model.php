<?php

class Team_model extends CI_Model
{
    private $table = "team";

    public function create($data){

        $this->db->insert($this->table, $data);
        return $this->db->insert_id();

    }

    public function delete($id) {
        $query = $this->db->where('id', $id)->delete($this->table);

        return $query;
    }

    public function update($id,$values){
        return $this->db->where('id', $id)->update($this->table, $values);
    }

    public function fetch($id){

        $query = $this->db->select('*')
            ->from($this->table)
            ->where('id',$id)
            ->get()
            ->row();

        return $query;
    }
    public function fetchAll(){

    $query = $this->db->select('*')
            ->from($this->table)
            ->get()
            ->result();

     return $query;
    }

    public function deleteTeamImageByFilename($filename)
    {
        $query = $this->db->where('filename', $filename)->update($this->table,array('filename' => ''));

        return $query;
    }

}