<?php

class Service_model extends CI_Model
{
    private $tableUserRequest = "userRequest";
    private $table = "service";
    private $tableColor = 'service_color';
    private $tableIcon = 'icons';
    private $tableIntercom = 'intercom_api';




    public function getUserRequestCount(){

        $query = $this->db->select('COUNT(DISTINCT(id)) as count')
            ->from($this->tableUserRequest)
            ->where('removed !=',1)
            ->where('viewed !=',1)
            ->get()
            ->row();

        if(!isset($query)) {
            return 0;
        }

        return $query->count;
    }
    public function creatUserRequest($data){

        $this->db->insert($this->tableUserRequest, $data);
        return $this->db->insert_id();
    }

    public function create($data){

        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }


    public function update($id,$values){
        return $this->db->where('id', $id)->update($this->table, $values);
    }

    public function fetch($id){

        $query = $this->db->select('*')
                        ->from($this->table)
                        ->where('id',$id)
                        ->get()
                        ->row();

        return $query;
    }
    public function fetchAll(){

        $query = $this->db->select('s.*,c.name as color')
            ->from($this->table." s")
            ->join($this->tableColor." c","s.color = c.id",'left')
            ->get()
            ->result();

        return $query;
    }

    public function fetchColor(){

        $query = $this->db->select('*')
            ->from($this->tableColor)
            ->get()
            ->result();

        return $query;
    }
    public function fetchIcon(){

        $query = $this->db->select('*')
            ->from($this->tableIcon)
            ->get()
            ->result();

        return $query;
    }

    public function deleteServiceImageByFilename($filename)
    {
        $query = $this->db->where('filename', $filename)->update($this->table,array('filename' => ''));

        return $query;
    }

    public function fetchIntercom(){
        $query = $this->db->select('*')
            ->from($this->tableIntercom)
            ->get()
            ->row();

        return $query;
    }

    public function createIntercom($data){
        $this->db->insert($this->tableIntercom, $data);
        return $this->db->insert_id();
    }

    public function updateIntercom($id,$values){
        return $this->db->where('id', $id)->update($this->tableIntercom, $values);
    }

    public function removeRequestUser($id,$values){
        return $this->db->where('id', $id)->update($this->tableUserRequest, $values);
    }
    public function delete($id)
    {
        $query = $this->db->where('id', $id)
            ->delete($this->table);

        return $query;
    }
}