<?php

class User_model extends CI_Model
{
    private $table = "users";
    
    public function create($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    public function deleteUser($id)
    {
        $query = $this->db->where('id', $id)
                    ->delete($this->table);

        return $query;
    }
  
    
    public function fetch($id)
    {
        $query = $this->db->select('*')
                            ->from($this->table)
                            ->where('id', $id)
                            ->get()
                            ->row();
        
        return $query;
                         
    }
    public function fetchByUser($id)
    {
        $query = $this->db->select('firstname,lastname,company,phone,type')
                            ->from($this->table)
                            ->where('id', $id)
                            ->get()
                            ->row();
        
        return $query;
                         
    }
    
    public function getUserByEmail($email) {
        $ret = $this->db->from($this->table)->where($email)->get();
        if ($ret->num_rows() == 0)
            return null;
        return $ret->first_row();
    }
   
    
    public function fetchByEmail($email)
    {
        $query = $this->db->select('*')
                            ->from($this->table)
                            ->where('email', $email)
                            ->get()
                            ->row();
        
        return $query;
    }
    
    public function fetchByKey($key)
    {
        $query = $this->db->select('*')
                            ->from($this->table)
                            ->where('confirmation_key', $key)
                            ->get()
                            ->row();
        
        return $query;
    }
    
    
    public function update($id, $values)
    {
        return $this->db->where('id', $id)->update($this->table, $values);
    }
    


    public function fetchByRememberKey($key)
    {
        $query = $this->db->select('*')
                        ->from($this->table)
                        ->where('remember_key', $key)
                        ->get()
                        ->row();

        return $query;
    }
    public function fetchByLogin($login)
    {
        $query = $this->db->select('*')
                            ->from($this->table)
                            ->where('email', $login)
                            ->get()
                            ->row();
        
        return $query;
    }
  
    

    
}
