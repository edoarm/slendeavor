<?php

class Partner_model extends CI_Model
{
    private $table = "partner";

    public function create($data){

        $this->db->insert($this->table, $data);
        return $this->db->insert_id();

    }

    public function delete($id) {
        $query = $this->db->where('id', $id)->delete($this->table);

        return $query;
    }

    public function update($id,$values){
        return $this->db->where('id', $id)->update($this->table, $values);
    }

    public function deletePartnerImageByFilename($filename)
    {
        $query = $this->db->where('filename', $filename)->delete($this->table);

        return $query;
    }


    public function fetchAll(){
       $query = $this->db->select('*')
                ->from($this->table)
                ->order_by('priority','DESC')
               ->get()
               ->result();

       return $query;
    }

}