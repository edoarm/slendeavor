<?php

class Gallery_model extends CI_Model
{
    private $table = "gallery";
    private $tableInfo = "gallery_info";
    private $tableImage = "gallery_img";
    
    public function create($data){

        $this->db->insert($this->table, $data);
        return $this->db->insert_id();

    }

    public function delete($id)
    {

        $this->db->where('id', $id)->delete($this->table);
        $this->db->where('gallery_id', $id)->delete($this->tableImage);
        $this->db->where('gallery_id', $id)->delete($this->tableImage);
//        return $query;
    }


    public function update($id,$values){
        return $this->db->where('id', $id)->update($this->table, $values);
    }

    public function createGalleryInfo($data){

        $this->db->insert($this->tableInfo, $data);
        return $this->db->insert_id();

    }

    public function deleteGalleryInfo($id)
    {
        $query = $this->db->where('id', $id)->delete($this->tableInfo);

        return $query;
    }
    public function deleteGalleryImageByFilename($filename)
    {
        $query = $this->db->where('filename', $filename)->delete($this->tableImage);

        return $query;
    }

    public function updateGalleryInfo($id,$values){
        return $this->db->where('id', $id)->update($this->tableInfo, $values);
    }


    public function createGalleryImage($data){

        $this->db->insert($this->tableImage, $data);
        return $this->db->insert_id();

    }

    public function deleteGalleryImage($id)
    {
        $query = $this->db->where('id', $id)->delete($this->tableImage);

        return $query;
    }

    public function updateGalleryImage($id,$values){
        return $this->db->where('id', $id)->update($this->tableImage, $values);
    }

    

    public function fetchAllById($id){

        $query = $this->db->select('*')
                ->from($this->table.' gl')
                ->where('id',$id)
                ->get()
                ->row();

        return $query;
    }
    public function fetchInfoById($id){

        $query = $this->db->select('*')
                ->from($this->tableInfo)
                ->where('gallery_id',$id)
                ->get()
                ->result();

        return $query;
    }
    public function fetchImageById($id){

        $query = $this->db->select('*')
                ->from($this->tableImage)
                ->where('gallery_id',$id)
                ->get()
                ->result();

        return $query;
    }
    public function fetchAllGallery(){
        $query = $this->db->select('gl.*,CONCAT("[", GROUP_CONCAT(CONCAT("{"\'"filename"\'":","\"",im.filename,"\"",,","),CONCAT(""\'"origin_name"\'":","\"",im.origin_name,"\"",","),CONCAT(""\'"main"\'":","\"",im.main,"\"","}")),"]") as images,'.
        '(SELECT CONCAT("[", GROUP_CONCAT(CONCAT("{"\'"title"\'":","\"",title,"\"",,","),CONCAT(""\'"text"\'":","\"",text,"\"","}")),"]") from gallery_info  WHERE gallery_id = gl.id) as info ')
            ->from($this->table.' gl')
            ->join($this->tableImage.' im','gl.id = im.gallery_id')
            ->group_by('im.gallery_id')
            ->get()
            ->result();

        return $query;
    }

}