<?php

class Works_model extends CI_Model
{
    private $table = "work";

    public function create($data){

        $this->db->insert($this->table, $data);
        return $this->db->insert_id();

    }

    public function delete($id) {
        $query = $this->db->where('id', $id)->delete($this->table);

        return $query;
    }

    public function update($id,$values){
        return $this->db->where('id', $id)->update($this->table, $values);
    }

    public function fetch($id){

        $query = $this->db->select('*')
            ->from($this->table)
            ->where('id',$id)
            ->get()
            ->row();

        return $query;
    }
    public function fetchAll(){

        $query = $this->db->select('*')
            ->from($this->table)
            ->get()
            ->result();

        return $query;
    }


}