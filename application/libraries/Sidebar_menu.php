<?php

class Sidebar_menu
{
    private $adminMenu = array(
        "Users" => array(
            'icon' => 'flaticon-list-1', 'active' => 0, 'children' => array(
                "Users" => array('icon' => "", 'active' => 0, 'actions' => array('users')),
                "New Users" => array('icon' => "", 'active' => 0, 'actions' => array('newUser')),
                
            )
           
        ),

        "Leads" => array(
            'icon' => 'flaticon-users', 'active' => 0, 'children' => array(
                "User Request" => array('icon' => "flaticon-paper-plane", 'active' => 0, 'actions' => array('userRequest') ),
                //                "User Request" => array('icon' => "flaticon-paper-plane", 'active' => 0, 'actions' => array('hardMoney'),'count' => 'getHardMoneyCount'),
            )
        ),
        "Intercom" => array(
            'icon' => 'flaticon-time-3', 'active' => 0, 'children' => array(
                "Intercom" => array('icon' => "flaticon-time-3", 'active' => 0, 'actions' => array('intercom')),
            )
        ),
        "Gallery" => array(
            'icon' => 'flaticon-line-graph', 'active' => 0, 'children' => array(
                "New" => array('icon' => "flaticon-paper-plane", 'active' => 0, 'actions' => array('gallery')),
                "List" => array('icon' => "flaticon-list-2", 'active' => 0, 'actions' => array('allGallery')),
            )
        ),
        "Partner" => array(
            'icon' => 'flaticon-map', 'active' => 0, 'children' => array(
                "List" => array('icon' => "flaticon-list-2", 'active' => 0, 'actions' => array('partners')),
            )
        ),
        "Services" => array(
            'icon' => 'flaticon-user-settings', 'active' => 0, 'children' => array(
                "New" => array('icon' => "flaticon-paper-plane", 'active' => 0, 'actions' => array('services')),
                "List" => array('icon' => "flaticon-list-2", 'active' => 0, 'actions' => array('allServices')),
            )
        ),
        "Team" => array(
            'icon' => 'flaticon-users', 'active' => 0, 'children' => array(
                "New" => array('icon' => "flaticon-paper-plane", 'active' => 0, 'actions' => array('team')),
                "List" => array('icon' => "flaticon-list-2", 'active' => 0, 'actions' => array('allTeam')),
            )
        ),
        "Works" => array(
            'icon' => 'flaticon-network', 'active' => 0, 'children' => array(
                "New" => array('icon' => "flaticon-paper-plane", 'active' => 0, 'actions' => array('works')),
                "List" => array('icon' => "flaticon-list-2", 'active' => 0, 'actions' => array('allWorks')),
            )
        ),

        "Manage Content " => array(
            'icon' => 'flaticon-cogwheel', 'active' => 0, 'children' => array(
                "Custom Texts" => array('icon' => "", 'active' => 0, 'actions' => array('customTexts')),


            )
        ),

    );
    private $userMenu = array(
        "User Requested" => array(
            'icon' => 'fa fa-user', 'active' => 0, 'children' => array(
                "Requested" => array('icon' => "fa fa-user", 'active' => 0, 'actions' => array('userRequest')),
            )
        ),

    );

    private function setActive($method, &$menu)
    {
        foreach($menu as $key => $item) {
            if(isset($item['children'])) {
                if($this->setActive($method, $menu[$key]['children'])) {
                    $menu[$key]['active'] = 1;
                    return true;
                };
            } else {
                if(in_array($method, $item['actions'])) {
                    $menu[$key]['active'] = 1;
                    return true;
                }
            }
        }
        return false;
    }


    public function getAdminMenu()
    {
        $ci =& get_instance();
        $method = $ci->router->method;
        $typeUser = $ci->session->userdata('type');
        if ($typeUser == 1) {
            $this->setActive($method, $this->adminMenu);
            return $this->adminMenu;
        }else if ($typeUser == 2) {
            $this->setActive($method, $this->userMenu);
            return $this->userMenu;
        }
    }


    public function getUserRequestCount()
    {
        $ci =& get_instance();
        $ci->load->model('service_model');
        $count = $ci->service_model->getUserRequestCount();
        if ($count == 0) {
            $count = "0";
        }
        return $count;
    }
    
}


