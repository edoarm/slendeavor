<?php

class Conf
{
    public static function getParam($key)
    {
        $CI =& get_instance();

        $CI->load->model('config_model');

        $value = $CI->config_model->getParam($key);

        return $value;
    }
}

