<?php

class Image_resizer
{
    private $allowedSizes = array(29,30,50,100,200,500);
    public function __construct()
    {
        
        $this->ci =& get_instance();
    }

    public function crop($filename, $x, $y, $width, $height) {

        $config['image_library'] = 'GD2';
        $config['source_image'] = ASSETSPATH.'/upload/original/'.$filename;
        $config['new_image'] = ASSETSPATH.'/upload/cropped/'.$filename;
        $config['maintain_ratio'] = FALSE;
        $config['width'] = $width;
        $config['height'] = $height;
        $config['y_axis'] = $y;
        $config['x_axis'] = $x;

        $this->ci->load->library('image_lib', $config);

        if (!$this->ci->image_lib->crop()) {
            return json_encode(array('status' => 'error',
                'message' => $this->ci->image_lib->display_errors()
            ));
        } else {
            return json_encode(array('status' => 'success',
                'url' => '/image/thumb/cropped/200/'.$filename
            ));
        }
    }

    public function createThumb($folder, $size, $filename) {

        $width = intval($size);

        if(!in_array($size, $this->allowedSizes)) {
            return false;
        }

        if(!is_dir(ASSETSPATH.'/upload/thumb_'.$size.'/')) {
            mkdir(ASSETSPATH.'/upload/thumb_'.$size.'/', 0777);
        }

        if(!file_exists(ASSETSPATH.'/upload/thumb_'.$size.'/'.$filename)) {

            $config['image_library'] = 'GD2';
            $config['source_image'] = ASSETSPATH.'/upload/'.$folder.'/'.$filename;
            $config['new_image'] = ASSETSPATH.'/upload/thumb_'.$size.'/'.$filename;
            $config['width'] = $width;

            $this->ci->load->library('image_lib', $config);
            $this->ci->image_lib->resize();
        }

        return ASSETSPATH.'/upload/thumb_'.$size.'/'.$filename;
    }

    public function makeSquare($folder, $filenames) {

        $size = 0;
        $errors = array();

        $this->ci->load->library('image_lib');

        if(count($filenames) != 0) {

            foreach($filenames as $filename) {

                $config['image_library'] = 'GD2';
                $config['y_axis'] = 0;
                $config['x_axis'] = 0;
                $config['maintain_ratio'] = FALSE;
                $config['source_image'] = ASSETSPATH.'/upload/'.$folder.'/'.$filename;
                $config['new_image'] = ASSETSPATH.'/upload/cropped/'.$filename;

                list($width, $height) = getimagesize($config['source_image']);
                if($width >= $height) {
                    $size = $height;
                } else {
                    $size = $width;
                }

                $config['width'] = $size;
                $config['height'] = $size;

                $this->ci->image_lib->clear();
                $this->ci->image_lib->initialize($config);
                if (!$this->ci->image_lib->crop()) {
                    $errors[] = $this->ci->image_lib->display_errors();
                } else {
                    $errors[] = "ok";
                }
            }
        }
    }
}