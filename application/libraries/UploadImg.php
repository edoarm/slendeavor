<?php

class UploadImg
{
    public function uploadImage()
    {
        $this->load->library('upload');

        if(!empty($_FILES)) {
            $filename = md5(uniqid(rand(), true));
            $config['upload_path'] = ASSETSPATH.'upload/upload/';
			$config['allowed_types'] = 'doc|txt|docx|pdf|jpg|jpeg|png';
			$config['file_name' ] = $filename;

            $files = $_FILES;

            $filesCount = count($_FILES['files']['name']);
            
            $result = array();
            
            for ($i = 0; $i < $filesCount; $i++)
			{
				$_FILES['files']['name'] = $files['files']['name'][$i];
				$_FILES['files']['type'] = $files['files']['type'][$i];
				$_FILES['files']['tmp_name'] = $files['files']['tmp_name'][$i];
				$_FILES['files']['error'] = $files['files']['error'][$i];
				$_FILES['files']['size'] = $files['files']['size'][$i];

				$this->upload->initialize($config);

				if (! $this->upload->do_upload("files")) {
                    
                    $result['files'][$i]['name'] = $this->upload->data('file_name');
                    $result['files'][$i]['size'] = $this->upload->data('file_size');
                    $result['files'][$i]['error'] = $this->upload->display_errors('','');
                    
                } else {
                    
                    $result['files'][$i]['name'] = $this->upload->data('orig_name');
                    $result['files'][$i]['size'] = $this->upload->data('file_size') * 1024;
                    $result['files'][$i]['type'] = $this->upload->data('file_type');
                    $result['files'][$i]['url'] = base_url('assets/upload/content/'.$this->upload->data('file_name'));
                    $result['files'][$i]['deleteUrl'] = base_url('upload/removeAttachment?file='.$this->upload->data('file_name'));
                    $result['files'][$i]['deleteType'] = 'DELETE';
                    $result['files'][$i]['uploadName'] = $this->upload->data('file_name');
                    $result['files'][$i]['thumbnailUrl'] = base_url('assets/upload/tmp/'.$this->upload->data('file_name'));
                }
			}
            
        }
        
//        echo json_encode($result);
        return $result;
    }
    
    public function removeUploadImage()
    {
        if($this->input->method(true) === 'DELETE') {
            
            $result = array();
            $filename = $this->input->get('file', true);
            $removed = unlink(ASSETSPATH. 'upload/upload/'.$filename);
            
            $result['files'][$filename] = $removed;
            
            echo json_encode($result);
            return;
        }
    }
}
