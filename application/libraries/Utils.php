<?php

class Utils
{
    public function generateRandomName($name = null) 
    {
        $mtime = str_replace(".","",microtime(true));
		$fileName = $mtime . "_" . md5($mtime.rand(1000000, 100000000));
		
		if($name != null){
			$fileName = $fileName . "." . strtolower(pathinfo($name, PATHINFO_EXTENSION));
		}
		
		return $fileName;
    }

	public function sendEmail($from, $to, $subject, $message, $reply = null, $html = true)
	{
		$CI =& get_instance();
		$CI->load->library('email');

		$email = $CI->email;

		$email->from($from);


		$email->to($to); 

		$email->subject($subject);

		if($html) {
			$email->set_mailtype("html");
		}

		$email->message($message);

		return $email->send();
	}

	public function sendEmailToQueue($from, $to, $subject, $message, $reply)
	{
		$CI =& get_instance();
		$CI->load->model('email_queue_model');

		// $from = "Notification From PortLoads <DoNotReply@portloads.com>";

		$CI->email_queue_model->create(array(
			'from_email'	=> $from,
			'to_email' 		=> $to,
			'subject' 		=> $subject,
			'message' 		=> $message,
			'reply_to' 		=> $reply
		));

		return true;
	}

	function sha1($str)
	{
		if ( ! function_exists('sha1'))
		{
			if ( ! function_exists('mhash'))
			{
				require_once(BASEPATH.'libraries/Sha1.php');
				$SH = new CI_SHA;
				return $SH->generate($str);
			}
			else
			{
				return bin2hex(mhash(MHASH_SHA1, $str));
			}
		}
		else
		{
			return sha1($str);
		}
	}

	public function sendEmailTemplate($from, $to, $templateName, $variables = array(), $realSend = false, $reply = null)
	{

		$templatePath = APPPATH."/emails/";

		$templateContent = file_get_contents($templatePath . "/" . $templateName . '.html');

		if(strpos($templateContent, "[--footer--]") !== false){
			$templateContent = str_replace("[--footer--]", file_get_contents($templatePath."/common/footer.html"), $templateContent);
		}

		foreach ($variables as $key => $value){
			$templateContent = str_replace("{{".$key."}}", $value, $templateContent);
		}

		list($subject,$message) = explode("[--subject--]", $templateContent);

		// if($realSend) {
			return $this->sendEmail($from, $to, $subject, $message, $reply);
		// }

		// return $this->sendEmailToQueue($from, $to, $subject, $message, $reply);
	}

	public function getEmailSubTemplate($subTemplateName, $variables = array())
	{
		$subTemplatePath = APPPATH."/emails/sub_templates";

		$subTemplateContent = file_get_contents($subTemplatePath. "/" . $subTemplateName . '.html');

		foreach ($variables as $key => $value){
			$subTemplateContent = str_replace("{{".$key."}}", $value, $subTemplateContent);
		}

		return $subTemplateContent;
	}
	
	public function cut_text($text = null, $numb_chars = 50) {
        if (!empty($text)) {
            $text = strip_tags($text);
            if (strlen($text) >= $numb_chars) {
                for ($i = 0; $i < 20; $i++) {
                    if (substr($text, $numb_chars, 1) != ' ') {
                        $numb_chars++;
                    } else {
                        break;
                    }
                }
                return substr($text, 0, $numb_chars) . '...';
            }
            return $text;
        }
    }

    public function uploadImage()
    {
        $CI =& get_instance();

        $CI->load->library('upload');

        if(!empty($_FILES)) {

            $config['upload_path'] = ASSETSPATH.'upload/content/';
            $config['allowed_types'] = 'doc|txt|docx|pdf|jpg|jpeg|png';

            $files = $_FILES;

            $filesCount = count($_FILES['files']['name']);

            $result = array();

            for ($i = 0; $i < $filesCount; $i++)
            {
                $config['file_name' ] = $this->generateRandomName($files['files']['name'][$i]);

                $_FILES['files']['name'] = $files['files']['name'][$i];
                $_FILES['files']['type'] = $files['files']['type'][$i];
                $_FILES['files']['tmp_name'] = $files['files']['tmp_name'][$i];
                $_FILES['files']['error'] = $files['files']['error'][$i];
                $_FILES['files']['size'] = $files['files']['size'][$i];

                $CI->upload->initialize($config);

                if (! $CI->upload->do_upload("files")) {

                    $result[$i]['name'] = $CI->upload->data('file_name');
                    $result[$i]['size'] = $CI->upload->data('file_size');
                    $result[$i]['error'] = $CI->upload->display_errors('','');

                } else {

                    $result[$i]['name'] = $CI->upload->data('orig_name');
                    $result[$i]['size'] = $CI->upload->data('file_size') * 1024;
                    $result[$i]['type'] = $CI->upload->data('file_type');
                    $result[$i]['url'] = base_url('assets/upload/content/'.$CI->upload->data('file_name'));
                    $result[$i]['uploadName'] = $CI->upload->data('file_name');
                }
			}
			
			
//            echo json_encode($result);
//            echo '<pre>';
//            print_r($result);die;
            return $result;
        }

    }

}

