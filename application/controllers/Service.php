<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends CI_Controller {


    public function __construct(){
        parent::__construct();
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $this->load->model('service_model');
        
    }

    public function viewedUserRequest(){

        $this->tryRedirect();
        $id = (int) $this->input->post('id',true);

        if (!empty($id)) {

            $this->service_model->removeRequestUser($id,array('viewed' => 1));
            echo json_encode(array('success' => true));

            return;
        }
    }

    public function removeRequestUser(){

        $this->tryRedirect();
        $id = (int) $this->input->post('id',true);
        if (!empty($id)){
            $this->service_model->removeRequestUser($id,array('removed' => 1));
            echo json_encode(array('success' => true));
            return;
        }

    }
    public function removeGallery(){

        $this->tryRedirect();
        $this->load->model('gallery_model');
        $id = (int) $this->input->post('id',true);
        if (!empty($id)){
            $this->gallery_model->delete($id);

            echo json_encode(array('success' => true));
            return;
        }
    }

    public function removeService(){

        $this->tryRedirect();
        $id = (int) $this->input->post('id',true);
        if (!empty($id)){
            $this->service_model->delete($id);

            echo json_encode(array('success' => true));
            return;
        }
    }
    public function removeWorks(){

        $this->tryRedirect();
        $this->load->model('works_model');
        $id = (int) $this->input->post('id',true);
        if (!empty($id)){
            $this->works_model->delete($id);

            echo json_encode(array('success' => true));
            return;
        }
    }
    public function removeTeam(){

        $this->tryRedirect();
        $this->load->model('team_model');
        $id = (int) $this->input->post('id',true);
        if (!empty($id)){
            $this->team_model->delete($id);

            echo json_encode(array('success' => true));
            return;
        }
    }

     public function updateTypeUser(){
             $this->tryRedirect();
             $this->load->model('user_model');

             $id = $this->input->post('id',true);
             $type = $this->input->post('type',true);
             
             $this->user_model->update($id,array('type' => $type));

             echo json_encode(array('success' => true));
             return;
         }


    public function removeGaleryImage(){

        $this->load->model('gallery_model');

        $filename = $this->input->post('filename',true);

        $this->gallery_model->deleteGalleryImageByFilename($filename);


        $image = str_replace(base_url('assets/upload/content/'),'',$filename);

        @unlink(ASSETSPATH.'/upload/content/'.$image);

        echo json_encode(array('success' => true));
    }

    public function removePartnersImage(){

        $this->load->model('partner_model');

        $filename = $this->input->post('filename',true);

        $this->partner_model->deletePartnerImageByFilename($filename);

        $image = str_replace(base_url('assets/upload/content/'),'',$filename);

        @unlink(ASSETSPATH.'/upload/content/'.$image);

        echo json_encode(array('success' => true));
    }

    public function removeServicesImage(){

        $this->load->model('service_model');

        $filename = $this->input->post('filename',true);

        $this->service_model->deleteServiceImageByFilename($filename);

        $image = str_replace(base_url('assets/upload/content/'),'',$filename);

        @unlink(ASSETSPATH.'/upload/content/'.$image);

        echo json_encode(array('success' => true));
    }

    public function removeTeamImage(){

        $this->load->model('team_model');

        $filename = $this->input->post('filename',true);

        $this->team_model->deleteTeamImageByFilename($filename);

        $image = str_replace(base_url('assets/upload/content/'),'',$filename);

        @unlink(ASSETSPATH.'/upload/content/'.$image);

        echo json_encode(array('success' => true));
    }



    public function removeUser(){
        $this->load->model('user_model');
        $id = $this->input->post('id',true);
        $this->user_model->update($id, array('removed' => 1));
        return;
    }



    private function loggedIn()
     {
         if($this->session->userdata('logged_in') && $this->session->userdata('admin')) {
             return true;
         }

         return false;
     }

     private function tryRedirect(){
           if(!$this->loggedIn()) {
               redirect(base_url('admin/login'));
           }
           return;
     }
}