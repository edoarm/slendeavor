<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    public function __construct(){
        parent::__construct();

//        header('Access-Control-Allow-Origin: http://www.slendeavor.com');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        header('Access-Control-Allow-Headers: Origin, Content-Type');
    }

    public function partners(){
        $this->load->model('partner_model');
        $partner = $this->partner_model->fetchAll();

        echo json_encode( $partner);
    }

    public function team(){
        $this->load->model('team_model');
        $team = $this->team_model->fetchAll();

        echo json_encode($team);

    }

    public function service(){

        $this->load->model('service_model');
        $service = $this->service_model->fetchAll();

        echo json_encode($service);
    }
    public function works(){

        $this->load->model('works_model');
        $works = $this->works_model->fetchAll();

        echo json_encode($works);

    }

    public function gallery(){

        $this->load->model('gallery_model');
        $gallery = $this->gallery_model->fetchAllGallery();
        echo json_encode($gallery);


    }

    public function requestForm(){

        $this->load->library('form_validation');
        $this->load->model('service_model');

        $name  = $this->input->post('name', true);
        $email = $this->input->post('email', true);
        $phone = $this->input->post('phone', true);

        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('phone', 'phone', 'required');

        if ($this->form_validation->run() !== FALSE) {

           $data = array(
               'name' => $name,
               'email' => $email,
               'phone' => $phone,
           );

           $this->service_model->creatUserRequest($data);

           echo json_encode(array('success' => true));
           return;
        }

        echo json_encode(array('error' => false));
        return;

    }
    public function sendEmail(){
        $this->load->library('form_validation');
        $this->load->library('utils');
        $email = $this->input->post('email', true);

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

        if ($this->form_validation->run() !== FALSE) {

            $this->utils->sendEmailTemplate($email, 'edul.avet@gmail.com',"email", array('email' => $email));

            echo json_encode(array('success' => true));
            return;
        }
        echo json_encode(array('error' => false));
        return;
    }

    public function intercome(){
        $this->load->model('service_model');
        $intercom = $this->service_model->fetchIntercom();

        echo json_encode($intercom);

    }


}