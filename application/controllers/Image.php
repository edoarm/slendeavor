<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Image extends CI_Controller
{
    function __construct() {

        parent::__construct();

        $this->load->library('image_resizer');
    }

    public function thumb($folder, $size, $filename = null) {
        print_r($filename);die;
		if(!file_exists(ASSETSPATH.'/upload/'.$folder.'/'.$filename) || empty($filename)) {

			$file = $this->image_resizer->createThumb($folder, $size, 'default.png');
		} else {
			$file = $this->image_resizer->createThumb($folder, $size, $filename);
		}

        $info = @getimagesize($file);

        header('Content-type: ' . $info['mime']);

        /*caching 3600 seconds*/
        header("Expires: ".gmdate("D, d M Y H:i:s", time() + 3600) . " GMT");
        header("Pragma: cache");
        header("Cache-Control: max-age=3600");
        /*caching*/

        readfile($file);
    }
}
