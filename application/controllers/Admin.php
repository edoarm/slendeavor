<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {


  public function __construct(){
      parent::__construct();
  }

  public function index(){

     $this->tryRedirect();
    $this->output->set_template('admin.phtml');
    $this->output->set_title('Slendeavor');
    $this->load->js('assets/admin/app/js/dashboard.js');

    $this->load->view('admin/default');

  }

  public function gallery($id = null){

      $this->tryRedirect();

      $this->output->set_template('admin.phtml');
      $this->output->set_title('gallery');

      $this->load->model('gallery_model');
      $this->load->js('assets/global/scripts/upload_image.js',
                    'assets/global/scripts/datatables-init.js',
                      'assets/global/scripts/common.js');

      if (empty($id)){

          $gallery = new stdClass();
          $info = array();
          $image = array();
          $gallery->id = '';
          $gallery->site_url = '';
          $gallery->site = '';
          $gallery->title = '';
          $gallery->video = '';
          $gallery->text = '';
          $gallery->info = '';

      }else{
          $gallery= $this->gallery_model->fetchAllById($id);
          $info= $this->gallery_model->fetchInfoById($id);
          $image= $this->gallery_model->fetchImageById($id);
      }

      $data['gallery'] = $gallery;
      $data['info'] = $info;
      $data['image'] = $image;
      
      $this->load->view('admin/gallery.phtml',$data);

  }

  public function saveGallery($galleryId = null){

      $this->tryRedirect();

      $this->load->library('utils');
      $this->load->model('gallery_model');

      $site_url = $this->input->post('site_url',true);
      $site = $this->input->post('site',true);
      $title = $this->input->post('title',true);
      $video = $this->input->post('video',true);
      $text = $this->input->post('text',true);
      $info_title = $this->input->post('info_title',true);
      $info_text = $this->input->post('info_text',true);
      $infoId = $this->input->post('info_id',true);
      $imageId = $this->input->post('image_id',true);
      $priority = $this->input->post('priority',true);
      $newTitle = $this->input->post('new_title',true);
      $mainImage = $this->input->post('main',true);

      if($this->input->method(true) === 'POST') {

          $data = array(
              'site_url' => $site_url,
              'site' => $site,
              'text' => $text,
              'video' => $video,
              'title' => $title,

          );

          if (empty($galleryId)){

              $galleryId = $this->gallery_model->create($data);

          }else{
              $this->gallery_model->update($galleryId,$data);
          }

        //save and remove info 
        $savedInfo = $this->gallery_model->fetchInfoById($galleryId);

        if($infoId) {

            foreach($infoId as $key => $id) {
                if($id == 0) {

                    $this->gallery_model->createGalleryInfo(array(
                        'title' => $info_title[$key],
                        'text' => $info_text[$key],
                        'gallery_id' => $galleryId));


                } else {
                    foreach($savedInfo as $key => $info) {
                        if($info->id == $id) {
                            $this->gallery_model->updateGalleryInfo($info->id,array(
                                'title' => $info_title[$key],
                                'text' => $info_text[$key]));
                            unset($savedInfo[$key]);
                            break;
                        }
                    }
                }
            }
            foreach($savedInfo as $info){
                $this->gallery_model->deleteGalleryInfo($info->id);
            }
        }

        //save and remove image 
        $savedImage = $this->gallery_model->fetchImageById($galleryId);

          $img = $this->utils->uploadimage();
          $count = 0;

        if($imageId) {

            foreach($imageId as $key => $id) {
                if($id == 0) {
                    $this->gallery_model->createGalleryImage(array(
                        'filename' => $img[$count]['url'],
                        'origin_name' => $newTitle[$key],
                        'priority' => $priority[$key],
                        'main' => $count == 0 ? '1' : '0',
                        'gallery_id' => $galleryId));
                 $count++;
                } else {
                    foreach($savedImage as $skey => $image) {
                        if($image->id == $id) {

                            $this->gallery_model->updateGalleryImage($image->id,array(
                                'origin_name' => $newTitle[$key],
                                'main' => $mainImage == $image->id ? '1' : "0",
                                'priority' => $priority[$key]));
                            unset($savedImage[$skey]);
                            break;
                        }
                    }
                }
            }
        }
          foreach($savedImage as $image){
              $this->gallery_model->deleteGalleryImage($image->id);
              unlink($image->filename);

          }


          if($this->input->is_ajax_request()) {
              echo json_encode(array( 'response'=> true, 'redirect' => base_url('admin/works/'.$galleryId)));
          } else {
              redirect(base_url('admin/gallery/'.$galleryId));
          }

      }
  }

  public function galleryGrid(){


      $this->tryRedirect();
      $this->onlyAdmin();

      $this->load->library('datatables');

      $this->load->helper('datatable');


      $this->datatables->select(' gl.id,gl.site_url,gl.site, gl.title,gl.text,gl.video,gi.filename,gi.main')
          ->from('gallery gl')
          ->join('gallery_img gi','gi.gallery_id = gl.id and gi.main = 1 '  ,'left')
          ->where('gi.main',1);

      $this->datatables->edit_column('filename', '$1', 'img_view_action(filename)');
      $this->datatables->edit_column('video', '$1', 'iframe_view_action(video)');
      $this->datatables->edit_column('date_added', '$1', 'users_date_requested_actions(date_added)');
      $this->datatables->add_column('actions', '$1', 'gallery_actions(id)');

      echo $this->datatables->generate();

  }
   public function allGallery(){
       $this->tryRedirect();
       $this->onlyAdmin();


       $this->output->set_template('admin.phtml');
       $this->load->js('assets/global/scripts/datatables-init.js');



       $this->load->view('admin/galleryGrid.phtml');

   }
  public function partners($id = null){

      $this->tryRedirect();

      $this->output->set_template('admin.phtml');
      $this->output->set_title('Partners');

      $this->load->model('partner_model');

      $this->load->js('assets/global/scripts/upload_image.js',
          'assets/global/scripts/common.js');

      $data['partner'] = $this->partner_model->fetchAll();


      $this->load->view('admin/partners.phtml',$data);
  }

  public function savepartners($id = null){

      $this->tryRedirect();

      $this->load->library('utils');
      $this->load->model('partner_model');
      $imageId = $this->input->post('image_id',true);
      $priority = $this->input->post('priority',true);
      $newTitle = $this->input->post('new_title',true);

        //save and remove image
      $savedImage = $this->partner_model->fetchAll();
      $img = $this->utils->uploadimage();
      $count = 0;
      if($imageId) {

          foreach($imageId as $key => $id) {
              if($id == 0) {

                  $this->partner_model->create(array(
                      'filename' => $img[$count]['url'],
                      'origin_name' => $newTitle[$key],
                      'priority' => $priority[$key]));
               $count++;
              } else {

                  foreach($savedImage as $skey => $image) {
                      if($image->id == $id) {
                          $this->partner_model->update($image->id,array(
                              'origin_name' => $newTitle[$skey],
                              'priority' => $priority[$skey]));
                          unset($savedImage[$skey]);
                          break;
                      }
                  }
              }
          }
      }

      foreach($savedImage as $image){
          $this->partner_model->delete($image->id);
          unlink(ASSETSPATH.'upload/content/'.$image->filename);

      }
      redirect(base_url('admin/partners/'));
  }

  public function services($id = null){

      $this->tryRedirect();

      $this->output->set_template('admin.phtml');
      $this->output->set_title('Services');

      $this->load->model('service_model');

      $this->load->js('assets/global/scripts/upload_image.js',
          'assets/admin/demo/default/custom/components/forms/validation/form-controls.js',
          'assets/global/scripts/common.js');

        if (empty($id)){
            $row = new stdClass();
            $row->id = '';
            $row->icon = '';
            $row->pre_title = '';
            $row->title = '';
            $row->text = '';
            $row->color = '';
            $row->img = '';


        }else{
            $row = $this->service_model->fetch($id);
        }

        $data['row'] = $row;
        $data['colors'] = $this->service_model->fetchColor();
        $data['icons'] = $this->service_model->fetchIcon();

      $this->load->view('admin/services.phtml',$data);

  }

    public function saveServices($id = null){

      $this->tryRedirect();
      $this->load->library('utils');
      $this->load->model('service_model');

      $icon = $this->input->post('icon',true);
      $title = $this->input->post('title',true);
      $text = $this->input->post('text',true);
      $preTitle = $this->input->post('pre_title',true);
      $color = $this->input->post('color',true);
      $newTitle = $this->input->post('new_title',true);
      $image = $this->input->post('image',true);
      $priority = $this->input->post('priority',true);
      $img = $this->utils->uploadimage();

      if($this->input->method(true) === 'POST') {

          $data = array(
              'icon' => $icon,
              'title' => $title,
              'text' => $text,
              'pre_title' => $preTitle,
              'color' => $color,
              'filename' => $img[0]['url'] ? $img[0]['url'] : $image,
              'origin_name' =>implode('',$newTitle),
              'priority' => implode('',$priority)

          );

          if (empty($id)){
              $id = $this->service_model->create($data);
          }else{
              $this->service_model->update($id,$data);
          }
          if($this->input->is_ajax_request()) {

              echo json_encode(array( 'response'=> true, 'redirect' => base_url('admin/works/'.$id)));

          } else {
              redirect(base_url('admin/services/'.$id));
          }

      }
  }
  public function servicesGrid(){

      $this->load->library('datatables');
      $this->load->helper('datatable');

      $this->datatables->select('id,icon,pre_title,title,text,filename')
          ->from('service');

      $this->datatables->edit_column('filename', '$1', 'img_view_action(filename)');
      $this->datatables->add_column('actions', '$1', 'services_actions(id)');

      echo $this->datatables->generate();

  }

  public function allServices(){

      $this->tryRedirect();
      $this->onlyAdmin();


      $this->output->set_template('admin.phtml');
      $this->load->js('assets/global/scripts/datatables-init.js');


      $this->load->view('admin/servicesGrid.phtml');

  }

  public function team($id = null){
      $this->tryRedirect();

      $this->output->set_template('admin.phtml');
      $this->output->set_title('Team');

      $this->load->model('team_model');

      $this->load->js('assets/global/scripts/upload_image.js',
          'assets/admin/demo/default/custom/components/forms/validation/form-controls.js',
          'assets/global/scripts/common.js');

      if (empty($id)){

          $row = new stdClass();
          $row->id = '';
          $row->info = '';
          $row->name = '';
          $row->origin_name = '';
          $row->filename = '';


      }else{
          $row = $this->team_model->fetch($id);
      }
      $data['row'] = $row;
      $this->load->view('admin/team.phtml',$data);

  }

  public function saveTeam($id = null){

      $this->load->library('utils');
      $this->load->model('team_model');

      $info = $this->input->post('info',true);
      $name = $this->input->post('name',true);
      $newTitle = $this->input->post('new_title',true);
      $priority = $this->input->post('priority',true);
      $image = $this->input->post('image',true);
      $img = $this->utils->uploadimage();

      if($this->input->method(true) === 'POST') {

          $data = array(
              'info' => $info,
              'name' => $name,
              'filename' => $img[0]['url'] ? $img[0]['url'] : $image,
              'origin_name' =>implode('',$newTitle),
              'priority' => implode('',$priority)

          );

          if (empty($id)){
              $id = $this->team_model->create($data);
          }else{
              $this->team_model->update($id,$data);
          }
          if($this->input->is_ajax_request()) {

              echo json_encode(array( 'response'=> true, 'redirect' => base_url('admin/team/'.$id)));

          } else {
              redirect(base_url('admin/team/'.$id));
          }

      }

  }

  public function teamGrid(){

      $this->load->library('datatables');
      $this->load->helper('datatable');

      $this->datatables->select('id,info,name,filename')
          ->from('team');

      $this->datatables->edit_column('filename', '$1', 'img_view_action(filename)');
      $this->datatables->add_column('actions', '$1', 'team_actions(id)');

      echo $this->datatables->generate();

  }

  public function allTeam(){

      $this->tryRedirect();
      $this->onlyAdmin();


      $this->output->set_template('admin.phtml');
      $this->load->js('assets/global/scripts/datatables-init.js');


      $this->load->view('admin/teamGrid.phtml');
  }

  public function works($id = null){
      $this->tryRedirect();


      $this->output->set_template('admin.phtml');
      $this->output->set_title('Works');

      $this->load->model('works_model');

      $this->load->js('assets/global/scripts/upload_image.js',
          'assets/admin/demo/default/custom/components/forms/validation/form-controls.js',
          'assets/global/scripts/common.js');

      if (empty($id)){
          $row = new stdClass();
          $row->id = '';
          $row->icon = '';
          $row->title = '';
          $row->text = '';


      }else{
          $row = $this->works_model->fetch($id);
      }
      $data['row'] = $row;

      $this->load->view('admin/works.phtml',$data);
  }

  public function saveWorks($id = null){
      $this->tryRedirect();

     $this->load->model('works_model');

     $icon = $this->input->post('icon',true);
     $title = $this->input->post('title',true);
     $text = $this->input->post('text',true);

      if($this->input->method(true) === 'POST') {

          $data = array(
              'icon' => $icon,
              'title' => $title,
              'text' => $text,
          );

          if (empty($id)){
              $id = $this->works_model->create($data);
          }else{
             $this->works_model->update($id,$data);
          }
          if($this->input->is_ajax_request()) {

              echo json_encode(array( 'response'=> true, 'redirect' => base_url('admin/works/'.$id)));

          } else {
              redirect(base_url('admin/works/'.$id));
          }

      }
  }

    public function worksGrid(){

        $this->load->library('datatables');
        $this->load->helper('datatable');

        $this->datatables->select('id,icon,title,text')
            ->from('work');

        $this->datatables->add_column('actions', '$1', 'works_actions(id)');

        echo $this->datatables->generate();

    }

    public function allWorks(){

        $this->tryRedirect();
        $this->onlyAdmin();


        $this->output->set_template('admin.phtml');
        $this->load->js('assets/global/scripts/datatables-init.js');


        $this->load->view('admin/worksGrid.phtml');
    }

  public function intercom(){
      $this->tryRedirect();
      $this->output->set_template('admin.phtml');
      $this->output->set_title('Intercome');

      $this->load->model('service_model');

      $row = $this->service_model->fetchIntercom();


      if (empty($row)){
          $row = new stdClass();
          $row->id = '';
          $row->user_id = '';
          $row->email = '';
          $row->name = '';
          $row->appID = '';

      }

      $data['row'] = $row;

      $this->load->view('admin/intercom.phtml',$data);

  }

    public function saveIntercom($id = null){
        $this->tryRedirect();

        $this->load->model('service_model');

        $userId = $this->input->post('userId',true);
        $email = $this->input->post('email',true);
        $name = $this->input->post('name',true);
        $appID = $this->input->post('appID',true);

        if($this->input->method(true) === 'POST') {

            $data = array(
                'userId' => $userId,
                'email' => $email,
                'name' => $name,
                'appID' => $appID,

            );

            if (empty($id)){
                $this->service_model->createIntercom($data);
            }else{
                $this->service_model->updateIntercom($id,$data);
            }
            if($this->input->is_ajax_request()) {

                echo json_encode(array( 'response'=> true, 'redirect' => base_url('admin/intercom/')));

            } else {
                redirect(base_url('admin/intercom/'));
            }

        }

    }

  public function customTexts()
  {

     $this->onlyAdmin();
     $this->tryRedirect();
     

      $this->output->set_template('admin.phtml');

      $this->output->set_title('LNDR');
      $this->load->css('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css');

      $this->load->js('assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js',
                      'assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js',
                      'assets/global/scripts/common.js');


      $this->load->view('admin/custom_texts.phtml');
  }

  public function saveCustomText()
  {
        $this->tryRedirect();
        $this->onlyAdmin();
        $this->load->library('utils');
        $this->load->model('config_model');
        if($this->input->method(true) === 'POST') {

          $key = $this->input->post('key', true);
          $value = $this->input->post('text');
          $image = $this->input->post('image', true);

          if(isset($image)) {

              $newFilename = $this->utils->generateRandomName($image);
              rename(ASSETSPATH.'upload/tmp/'.$image, ASSETSPATH.'upload/content/'.$newFilename);
              $value = $newFilename;
          }

          $this->config_model->setParam($key, $value);
      }

      redirect(base_url('admin/customTexts'));
  }


    public function userRequest(){

        $this->tryRedirect();
        $this->onlyAdmin();

        $this->output->set_template('admin.phtml');
        $this->load->js('assets/global/scripts/datatables-init.js');

        $this->load->view('admin/userRequest.phtml');
    }
    public function userRequsetGrid(){

        $this->tryRedirect();
        $this->onlyAdmin();

        $this->load->library('datatables');

        $this->load->helper('datatable');

        $this->datatables->select('hm.id,hm.name, hm.phone,hm.email,hm.date_request,hm.viewed')
            ->where('removed !=',1)
            ->from('userRequest hm');

        $this->datatables->edit_column('date_added', '$1', 'users_date_requested_actions(date_added)');
        $this->datatables->edit_column('viewed', '$1', 'users_viwed_requested_actions(viewed)');
        $this->datatables->add_column('actions', '$1', 'users_request_actions(id)');

        echo $this->datatables->generate();



    }


    public function login()
    {
        $this->output->unset_template();
        $this->load->model('user_model');


        $this->load->helper('cookie');
            
          $login = $this->input->post('username', true);
          $password = $this->input->post('password', true);
          $remember = $this->input->post('remember', true);
          if($this->input->method(true) == 'POST') {

          $user = $this->user_model->fetchByEmail($login);

            if(!empty($user) && password_verify($password, $user->password)) {

                if(empty($user->confirmation_key)) {

                    $this->loginUser($user->id);
                    return;

                } else {
                    echo json_encode(array('error' => 'your email is not confirmed'));

                }

            } else {
                echo json_encode(array('error' => 'wrong email or password'));

            }
          }

          $this->load->view('admin/login');
    }

    public function users()
    {
        $this->tryRedirect();
          $this->onlyAdmin();


        $this->output->set_template('admin.phtml');

          $this->load->js('assets/global/scripts/datatables-init.js');



        $this->load->view('admin/users.phtml');
    }

    public function usersGrid()
    {
        $this->tryRedirect();
        $this->onlyAdmin();

        $this->load->library('datatables');
        
        $this->load->helper('datatable');
        $this->datatables->select('u.id, u.firstname, u.lastname,u.date_added, u.phone, u.type,')
                        ->where('removed !=',1)
                        ->from('users u');

        $this->datatables->edit_column('date_added', '$1', 'users_date_requested_actions(date_added)');
        $this->datatables->add_column('actions', '$1', 'users_actions(id, type)');
        
        echo $this->datatables->generate();
        return;
    }

    public function newUser(){

        $this->tryRedirect();
        $this->onlyAdmin();
        
       $this->output->set_template('admin.phtml');

        $this->load->js('assets/global/scripts/form-admin.js');

       $this->load->view('admin/new_user.phtml');

    }


    public function signup($id = null){
    $this->tryRedirect();
    
    $this->onlyAdmin();

    $this->output->unset_template();

    $this->load->library('utils');
    $this->load->library('form_validation');
    $this->load->model('user_model');

    $firstname = $this->input->post('firstname', true);
    $lastname = $this->input->post('lastname', true);
    $password = $this->input->post('pass', true);
    $phone = $this->input->post('phone', true);
    $type = (int) $this->input->post('type', true);

    $this->form_validation->set_rules('firstname', 'Firstname', 'required');
    $this->form_validation->set_rules('lastname', 'Lastname', 'required');
    $this->form_validation->set_rules('pass', 'Password', 'required|min_length[6]');
    $this->form_validation->set_rules('cpass', 'Password confirm', 'required|matches[pass]');
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
    $this->form_validation->set_rules('phone', 'Phone', 'required');
    $this->form_validation->set_rules('type', 'type', 'required');

    if($this->form_validation->run() !== false) {
        
        $email = $this->input->post('email', true);
        
        if($this->userExists($email)) {

            $this->session->set_flashdata('message', 'User already exists');
            $this->session->set_flashdata('message_type', 'danger');
            redirect(base_url('admin/newUser'));
            return;
            
        } else {

            $passwordBinary = gzcompress(base64_encode($password));

            

            $userId = $this->user_model->create(array('firstname' => ucfirst($firstname),
                                                       'lastname' => ucfirst($lastname),
                                                       'email' => $email,
                                                       'phone' => $phone,
                                                       'type' => $type,
                                                       'password' => password_hash($password, PASSWORD_DEFAULT),
                                                       'tp' => $passwordBinary));

            if($userId) {

                $user = $this->user_model->fetch($userId);
                $key = password_hash($user->id . $user->email, PASSWORD_DEFAULT);

                $activationLink = base_url('admin/activate?k='.$key.'&login=true');
                $username = $user->firstname;
                $passUser = base64_decode(gzuncompress($user->tp));

                $this->utils->sendEmailTemplate("info@mylndr.com", $user->email, "registration", array('link' => $activationLink, 'username' => $username,'password' => $passUser,'email'=> $email));

                $this->user_model->update($userId, array('confirmation_key' => $key,'tp' => null));

                  $this->session->set_flashdata('message', 'success');
                  $this->session->set_flashdata('message_type', 'success');
                  redirect(base_url('admin/newUser'));
            }
        }
        
    }else {
      $this->session->set_flashdata('message', 'incorrect form data');
      $this->session->set_flashdata('message_type', 'danger');
      redirect(base_url('admin/newUser'));
      return;
    }
  }
  

  public function activate()
  {
      $this->output->unset_template();
     $this->load->model('user_model');
      
      $key = $this->input->get('k');
      $user = $this->user_model->fetchByKey($key);
      
      if(isset($user)){
          
          if($user->confirmation_key == $key && password_verify($user->id.$user->email,$key)){
              
              $this->user_model->update($user->id, array('confirmation_key' => null));
              
              $this->loginUser($user->id);
          }
      }
  }
  public function newPasswordActivate()
  {
      $this->output->unset_template();
      $this->load->model('user_model');
      
      $key = $this->input->get('k');
      $user = $this->user_model->fetchByForgotKey($key);
      
      if(isset($user)){
          
          if($user->new_password_key == $key && password_verify($user->id.$user->email,$key)){
              
              // $this->user_model->update($user->id, array('new_password_key' => null));
              
              $this->loginUser($user->id);
          }
      }
  }
    
  public function success()
  {  
      $this->load->view('account/success');
  }
  
  public function ForwardPassword(){

      $this->output->unset_template();

      $this->load->library('form_validation');

      $this->load->library('utils');

      $this->form_validation->set_rules('email', 'email','trim|required');
      
      if(!$this->form_validation->run()){
          return redirect(base_url());
      }
      $mail_post =  $this->input->post('email',true);

      $pass =  $this->user_model->getUserByEmail(array('email' => $mail_post));

      if(empty($pass)){
          return redirect( base_url());
      }

      $key = password_hash($pass->id . $pass->email, PASSWORD_DEFAULT);

      $activationLink = base_url('account/newPasswordActivate?k='.$key);
   
      $username = $pass->firstname;

      $this->utils->sendEmailTemplate("info@portloads.com", $pass->email, "forward", array('link' => $activationLink, 'username' => $username));

      $this->user_model->update($pass->id,array("new_password_key" => $key));
      
       echo json_encode(array('type' => 'success', 'message' => 'Please check your email for instructions'));
      
      
  }
  public function newPasswordSave()
  {
      $this->output->unset_template();

      if($this->input->method(true) == 'POST') {

          $this->form_validation->set_rules('password', 'Password', 'required');
          $this->form_validation->set_rules('c_password', 'Confirm Password', 'required|matches[password]');

          if($this->form_validation->run() !== false) {
              $password = $this->input->post('password',true);
              $userid = $this->session->userdata('id');
              $user = $this->user_model->fetch($userid);

              $this->user_model->update($userid,array('password' => password_hash($password, PASSWORD_DEFAULT),'new_password_key' => null));
         }

      }
      $this->loginUser($user->id);
      
  }

 public function logout()
    {
    session_destroy();

    redirect(base_url());
  }

 private function loggedIn()
  {
      if($this->session->userdata('logged_in') && $this->session->userdata('admin')) {
          return true;
      }

      return false;
  }
  
  private function  onlyAdmin(){
    if($this->session->userdata('type') === '1') {
        return true;
    }
    redirect(base_url('admin'));
    
  }

  private function tryRedirect()
    {
        if(!$this->loggedIn()) {
            redirect(base_url('admin/login'));
        }
        return;
    }
  private function userExists($email)
  {

      $user = $this->user_model->fetchByEmail($email);
      
      if(isset($user)) {
          return true;
      }

      return false;
  }
  private function loginUser($userId)
  {
      $user = $this->user_model->fetch($userId);

          $this->session->set_userdata(array(
              'id'        => $user->id,
              'firstname' => $user->firstname,
              'lastname'  => $user->lastname,
              'type'    => $user->type,
              'logged_in' => true,
              'admin' => true));

      if($this->input->is_ajax_request()) {
          echo json_encode(array( 'response'=> true, 'redirect' => base_url('admin')));
      } else {
          redirect(base_url('admin'));
      }
      return;
  }
}
