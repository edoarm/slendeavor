<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('admin_actions')){

    function users_actions($id, $type)    {
        $html ='<a href="'.base_url('admin/userEdit/'.$id).'" class="btn btn-sm btn-block m-btn--square btn-accent"><i class="flaticon-edit"></i> Edit</a>'.

            '<a href="#change_status" class="btn btn-sm btn-block m-btn--square btn-focus userStatus" data-toggle="modal" idStarus="'.$id.'"><i class="flaticon-statistics" aria-hidden="true"></i> Type</a>'.
            '<a href="#small" class="btn btn-sm btn-block m-btn--square btn-danger removeuser" data-toggle="modal" data-id="'.$id.'"><i class="flaticon-delete" aria-hidden="true"></i> Delete</a>';
        return $html;
    }

    function users_request_actions($id)    {
        $html = '<a href="javascript:" target="_blank" data-id="'.$id.'" class="btn btn-sm btn-block  view_user_requested m-btn--square btn-info"><i class="flaticon-visible"></i> View</a>'.
            '<a href="javascript:" data-target="#small" class="btn btn-sm btn-block  m-btn--square btn-danger userDeledeRequest" data-toggle="modal" data-id="'.$id.'"><i class="flaticon-delete" aria-hidden="true"></i> Delete</a>';
        return $html;
    }

    function users_date_requested_actions($date_requested)    {
        return  $date_requested = '<div>'.date("m/d/y - h:i a", strtotime($date_requested)).'</div>';
    }

    function users_viwed_requested_actions($viewed)    {

        switch($viewed) {
            case 0:
                $type = '<span style="color:#89C4F4;font-size:18px;">New</span>';
            break;
            case 1:
                $type = '<i class="fa fa-check" style="color:green;font-size:18px;" aria-hidden="true"></i>';
            break;
            default:
                $type = null;
        }
        return '<p class="text-center">'.$type.'</p>';

    }

    function gallery_actions($id){

        $html = '<a href="'.base_url('admin/gallery/'.$id).'" target="_blank" data-id="'.$id.'" class="btn btn-sm btn-block m-btn--square btn-accent"><i class="flaticon-edit"></i> Edit</a>'.
            '<a href="#small" class="btn btn-sm btn-block m-btn--square btn-danger galeryRemove" data-toggle="modal" data-id="'.$id.'"><i class="flaticon-delete" aria-hidden="true"></i> Delete</a>';
        return $html;

    }
    function services_actions($id){

        $html = '<a href="'.base_url('admin/services/'.$id).'" target="_blank" data-id="'.$id.'" class="btn btn-sm btn-block m-btn--square btn-accent"><i class="flaticon-edit"></i> Edit</a>'.
            '<a href="#small" class="btn btn-sm btn-block m-btn--square btn-danger servicesRemove" data-toggle="modal" data-id="'.$id.'"><i class="flaticon-delete" aria-hidden="true"></i> Delete</a>';
        return $html;

    }

    function team_actions($id){

        $html = '<a href="'.base_url('admin/team/'.$id).'" target="_blank" data-id="'.$id.'" class="btn btn-sm btn-block m-btn--square btn-accent"><i class="flaticon-edit"></i> Edit</a>'.
            '<a href="#small" class="btn btn-sm btn-block m-btn--square btn-danger teamRemove" data-toggle="modal" data-id="'.$id.'"><i class="flaticon-delete" aria-hidden="true"></i> Delete</a>';
        return $html;

    }

    function works_actions($id){

        $html = '<a href="'.base_url('admin/works/'.$id).'" target="_blank" data-id="'.$id.'" class="btn btn-sm btn-block m-btn--square btn-accent"><i class="flaticon-edit"></i> Edit</a>'.
            '<a href="#small" class="btn btn-sm btn-block m-btn--square btn-danger worksRemove" data-toggle="modal" data-id="'.$id.'"><i class="flaticon-delete" aria-hidden="true"></i> Delete</a>';
        return $html;

    }

    function img_view_action($img){
        $html = '<img height="100" width="100" style="object-fit: contain;" src="'.$img.'">';
        return $html;

    }
    function iframe_view_action($iframe){
        $html = '<iframe src="'.$iframe.'" height="100" width="100%"></iframe>';
        return $html;

    }

    function hard_money_actions($id){
        $html = '<a href="'.base_url('admin/hardMoneyView/'.$id).'" target="_blank" data-id="'.$id.'" class="btn btn-sm hardMoneyView m-btn--square btn-info"><i class="	flaticon-visible"></i> View</a>'.
            '<a href="#small" class="btn btn-xs btn-block m-btn--square hardMoneyRemove" data-toggle="modal" data-id="'.$id.'"><i class="flaticon-delete" aria-hidden="true"></i> Delete</a>';
        return $html;
    }



}