//== Class definition

var DatatableJsonRemote = function () {
	//== Private functions

    function gonfig(source) {
        if (source){
            var query = source.getDataSourceQuery();
            $('#m_form_search').on('keyup', function (e) {
                source.search($(this).val().toLowerCase());
            }).val(query.generalSearch);

            $('#m_form_status').on('change', function () {
                source.search($(this).val(), 'Status');
            }).val(typeof query.Status !== 'undefined' ? query.Status : '');

            $('#m_form_type').on('change', function () {
                source.search($(this).val(), 'Type');
            }).val(typeof query.Type !== 'undefined' ? query.Type : '');

            $('#m_form_status, #m_form_type').selectpicker();
        }

    }
	// basic demo
	var user = function () {

		var datatable = $('#ajax_user').mDatatable({
			// datasource definition
			data: {
				type: 'remote',
				source: '/admin/usersGrid',
				pageSize: 10,
				saveState: {
					cookie: true,
					webstorage: true
				}
			},
			// layout definition
			layout: {
				theme: 'default', // datatable theme
				class: '', // custom wrapper class
				scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
				height: 550, // datatable's body's fixed height
				footer: false // display/hide footer
			},

			// column sorting
			sortable: true,
			// column based filtering
			filterable: false,
			pagination: true,
			// columns definition
			columns: [{
				field: "firstname",
				title: "Firstname"
			}, {
				field: "lastname",
				title: "Lastname",
			}, {
				field: "phone",
				title: "Phone",
				width: 110
			}, {
				field: "date_added",
				title: "Date Added",
				responsive: {visible: 'lg'}
			},{
                field: "type",
                title: "Type",
                template: function (row) {
                    var text = '';
                    switch(row.type) {
                        case '1': text = "Admin"; break;
                        case '2': text = "User"; break;
                        default: break;
                    }
                    return '<span class="label label-sm label-info">'+text+'</span>';
                }
            },{
				field: "actions",
				width: 110,
				title: "Actions",
				sortable: false,
				overflow: 'visible',
				// template: function (row) {
                 //    var dropup = (row.getDatatable().getPageSize() - row.getIndex()) <= 4 ? 'dropup' : '';
                //
				// 	var html = '<div class="dropdown \'+ dropup +\'">'+
				// 			'<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">'+
                 //                '<i class="la la-ellipsis-h"></i>'+
                 //            '</a>'+
				// 			'<div class="dropdown-menu dropdown-menu-right">'+
				// 				$.each(row.actions, function (index, value) {
                 //                    return value;
                 //                });
                //
                //
                 //    '</div>'+
                 //            row.actions
				// 	'</div>';
                //
                 //    return html;
				// }
			}]
		});
        gonfig(datatable);

	};
    var galery = function () {

        var datatable = $('#ajax_galery').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: '/admin/galleryGrid',
                pageSize: 10,
                saveState: {
                    cookie: true,
                    webstorage: true
                }
            },

            // layout definition
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            // column sorting
            sortable: true,
            // column based filtering
            filterable: false,
            pagination: true,
            // columns definition
            columns: [{
                field: "site_url",
                title: "Site Url"
            }, {
                field: "title",
                title: "Title",
            }, {
                field: "video",
                title: "Video",
            }, {
                field: "filename",
                title: "Image",
            },{
                field: "actions",
                width: 110,
                title: "Actions",
                sortable: false,
                overflow: 'visible',
            }]
        });


        gonfig(datatable);

    };

    var services = function () {

        var datatable = $('#ajax_services').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: '/admin/servicesGrid',
                pageSize: 10,
                saveState: {
                    cookie: true,
                    webstorage: true
                }
            },

            // layout definition
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            // column based filtering
            filterable: false,

            pagination: true,
            // columns definition
            columns: [{
                field: "icon",
                title: "Icon"
            }, {
                field: "pre_title",
                title: "Pre title",
            }, {
                field: "title",
                title: "Title",
            }, {
                field: "text",
                title: "Text",
            }, {
                field: "filename",
                title: "image",
            },{
                field: "actions",
                width: 110,
                title: "Actions",
                sortable: false,
                overflow: 'visible',
            }]
        });
        gonfig(datatable);
    };

    var team = function () {

        var datatable = $('#ajax_team').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: '/admin/teamGrid',
                pageSize: 10,
                saveState: {
                    cookie: true,
                    webstorage: true
                }
            },

            // layout definition
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            // column based filtering
            filterable: false,

            pagination: true,
            // columns definition
            columns: [{
                field: "info",
                title: "Info"
            }, {
                field: "name",
                title: "Name",
            }, {
                field: "filename",
                title: "image",
            },{
                field: "actions",
                width: 110,
                title: "Actions",
                sortable: false,
                overflow: 'visible',
            }]
        });
        gonfig(datatable);

    };
    var works = function () {

        var datatable = $('#ajax_work').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: '/admin/worksGrid',
                pageSize: 5,
                saveState: {
                    cookie: true,
                    webstorage: true
                }
            },

            // layout definition
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            // column based filtering
            filterable: false,

            pagination: true,
            // columns definition
            columns: [{
                field: "title",
                title: "Title"
            }, {
                field: "icon",
                title: "Icon",
            }, {
                field: "text",
                title: "Text",
            },{
                field: "actions",
                width: 110,
                title: "Actions",
                sortable: false,
                overflow: 'visible',
            }]
        });
        gonfig(datatable);

    };


    return {
		// public functions
        init: function () {
			user();
            galery();
            services();
            team();
            works();
        }
	};
}();

jQuery(document).ready(function () {
    DatatableJsonRemote.init();
});