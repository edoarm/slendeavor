//== Class definition

var FormControls = function () {
    //== Private functions

    var gallery = function () {

        $('#gallery_form').validate({
            rules:{
                site_url: {
                    required: true,
                },
                site: {
                    required: true
                },
                title: {
                    required: true,
                },
                video: {
                    required: true,
                },

            },

            invalidHandler: function(event, validator) {
                var alert = $('#m_form_1_msg');
                alert.removeClass('m--hide').show();
                mApp.scrollTo(alert, -200);
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }

        });
    };
    var services = function () {

        $('#services_form').validate({
            rules:{
                icon: {
                    required: true,
                },
                pre_title: {
                    required: true
                },
                title: {
                    required: true,
                },
                text: {
                    required: true,
                },
                color: {
                    required: true,
                },

            },

            invalidHandler: function(event, validator) {
                var alert = $('#m_form_1_msg');
                alert.removeClass('m--hide').show();
                mApp.scrollTo(alert, -200);
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }

        });
    };
    var team = function () {

        $('#team_form').validate({
            rules:{
                info: {
                    required: true,
                },
                name: {
                    required: true
                },
            },

            invalidHandler: function(event, validator) {
                var alert = $('#m_form_1_msg');
                alert.removeClass('m--hide').show();
                mApp.scrollTo(alert, -200);
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }

        });


    };
    var works = function () {

        $('#works_form').validate({
            rules:{
                title: {
                    required: true,
                },
                icon: {
                    required: true
                },
                text: {
                    required: true
                },
            },

            invalidHandler: function(event, validator) {
                var alert = $('#m_form_1_msg');
                alert.removeClass('m--hide').show();
                mApp.scrollTo(alert, -200);
            },

            submitHandler: function (form) {
                // getWorks(form);
                form[0].submit(); // submit the form
            }

        });

        function getWorks(form) {
            $form = $(form);

            var data = $form.serialize();
            var url = $form.attr('action');

            $.ajax({
                url: url,
                data:data,
                method:'post',
                dataType:'json',
                success: function(response, status, xhr, $form) {
                    // console.log(response);
                    // var data = JSON.parse(response);
                    if (response.response){
                        window.location.href = response.redirect

                    }else{

                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                        showErrorMsg(form, 'danger', '"'+data.error+'" Please try again.');
                    }
                }
            })

        }
    };

    var demo1 = function () {
        $( "#m_form_1" ).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true 
                },
                url: {
                    required: true 
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true 
                },
                phone: {
                    required: true,
                    phoneUS: true 
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $('#m_form_1_msg');
                alert.removeClass('m--hide').show();
                mApp.scrollTo(alert, -200);
            },

            submitHandler: function (form) {
                //form[0].submit(); // submit the form
            }
        });       
    };

    var demo2 = function () {
        $( "#m_form_2" ).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true 
                },
                url: {
                    required: true 
                },
                digits: {
                    required: true,
                    digits: true
                },
                creditcard: {
                    required: true,
                    creditcard: true 
                },
                phone: {
                    required: true,
                    phoneUS: true 
                },
                option: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },

                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $('#m_form_2_msg');
                alert.removeClass('m--hide').show();
                mApp.scrollTo(alert, -200);
            },

            submitHandler: function (form) {
                //form[0].submit(); // submit the form
            }
        });       
    };

    return {
        // public functions
        init: function() {
            demo1(); 
            demo2();
            gallery();
            services();
            team();
            works();
        }
    };
}();

jQuery(document).ready(function() {    
    FormControls.init();
});