/***
Wrapper/Helper Class for datagrid based on jQuery Datatable Plugin
***/
var Datatable = function() {

    var tableOptions; // main options
    var dataTable; // datatable object
    var table; // actual table jquery object
    var tableContainer; // actual table container object
    var tableWrapper; // actual table wrapper jquery object
    var tableCustomFilterWrapper;
    var tableSearchAllWrapper;
    var tableInitialized = false;
    var ajaxParams = {}; // set filter mode
    var the;
    var filterParams = {};
    var customFilterParams = {};
    var filterAllParam = '';

    var countSelectedRecords = function() {
        var selected = $('tbody > tr > td:nth-child(1) input[type="checkbox"]:checked', table).size();
        var text = tableOptions.dataTable.language.metronicGroupActions;
        if (selected > 0) {
            $('.table-group-actions > span', tableWrapper).text(text.replace("_TOTAL_", selected));
        } else {
            $('.table-group-actions > span', tableWrapper).text("");
        }
    };

    return {

        //main function to initiate the module
        init: function(options) {

            if (!$().dataTable) {
                return;
            }

            the = this;

            // default settings
            options = $.extend(true, {
                src: "", // actual table  
                filterApplyAction: "filter",
                filterCancelAction: "filter_cancel",
                resetGroupActionInputOnSuccess: true,
                loadingMessage: 'Loading...',
                dataTable: {
                    "dom": "<'row'<'col-md-offset-7 col-md-5 col-sm-12'<'table-search-all pull-right'>>r><'table-scrollable't><'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>", // datatable layout
                    "pageLength": 10, // default records per page
                    "language": { // language settings
                        // metronic spesific
                        "metronicGroupActions": "_TOTAL_ records selected:  ",
                        "metronicAjaxRequestGeneralError": "Could not complete request. Please check your internet connection",

                        // data tables spesific
                        "lengthMenu": "<span class='seperator'>|</span>View _MENU_ records",
                        "info": "<span class='seperator'>|</span>Found total _TOTAL_ records",
                        "infoEmpty": "No records found to show",
                        "emptyTable": "No data available in table",
                        "zeroRecords": "No matching records found",
                        "paginate": {
                            "previous": "Prev",
                            "next": "Next",
                            "last": "Last",
                            "first": "First",
                            "page": "Page",
                            "pageOf": "of"
                        }
                    },

                    "orderCellsTop": true,
                    "columnDefs": [
                    // { // define columns sorting options(by default all columns are sortable extept the first checkbox column)
                    //     'orderable': false,
                    //     'searchable': false,
                    //     'targets': [0],
                    //     'render': function(data, type, row){
                    //         return '<input type="checkbox" name="id[]" value="'+row.id+'">';
                    //     },
                    // },
					{// expand functionality for actions column
						'targets': [-1],
						'orderable': false,
						'searchable': false,
						'render': function(data, type, row){

							var $actions = $(data);

							if($actions.length > 2) {

								var $html = $('<div class="actions-content"></div>');
								var $expandButton = $('<div class="text-center actions-expand-button"><a href="#" class="btn btn-xs grey-cascade btn-expand" title="Show more actions"><i class="fa fa-angle-double-down"></i></a></div>');

								$actions.each(function(key, value){

									if(key > 1) {
										$(value).addClass('action-invisible');

									} else {
										var $icon = $(value).find('i');
										$(value).css({'display':'inline-block','width':'auto'})
										$(value).attr('title', $(value).text());
										$(value).empty().append($icon);
									}

									$html.append(value);
								});

								$html.append($expandButton);
								return $html.get(0).outerHTML;
							}

							return data;
						}
					}
                    ],

                    "pagingType": "bootstrap_extended", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
                    "autoWidth": false, // disable fixed width and enable fluid table
                    "processing": false, // enable/disable display message box on record load
                    "serverSide": true, // enable/disable server side ajax loading

                    "ajax": { // define ajax settings
                        "url": "", // ajax URL
                        "type": "POST", // request type
                        "timeout": 20000,
                        "data": function(data) { // add request parameters before submit
                            $.each(ajaxParams, function(key, value) {
                                data[key] = value;
                            });
                            $.each(filterParams, function(key, value) { //overwrites native search parameters
                                data['columns'][key]['search'] = value;
                            });
                            
                            data['custom'] = [];
                            
                            data['custom'].push(customFilterParams);

                            data['search']['value'] = filterAllParam;
                            
                            Metronic.blockUI({
                                message: tableOptions.loadingMessage,
                                target: tableContainer,
                                overlayColor: 'none',
                                cenrerY: true,
                                boxed: true
                            });
                        },
                        "dataSrc": function(res) { // Manipulate the data returned from the server
                            if (res.customActionMessage) {
                                Metronic.alert({
                                    type: (res.customActionStatus == 'OK' ? 'success' : 'danger'),
                                    icon: (res.customActionStatus == 'OK' ? 'check' : 'warning'),
                                    message: res.customActionMessage,
                                    container: tableWrapper,
                                    place: 'prepend'
                                });
                            }

                            if (res.customActionStatus) {
                                if (tableOptions.resetGroupActionInputOnSuccess) {
                                    $('.table-group-action-input', tableWrapper).val("");
                                }
                            }

                            if ($('.group-checkable', table).size() === 1) {
                                $('.group-checkable', table).attr("checked", false);
                                $.uniform.update($('.group-checkable', table));
                            }

                            if (tableOptions.onSuccess) {
                                tableOptions.onSuccess.call(undefined, the);
                            }

                            Metronic.unblockUI(tableContainer);

                            return res.data;
                        },
                        "error": function() { // handle general connection errors
                            if (tableOptions.onError) {
                                tableOptions.onError.call(undefined, the);
                            }

                            Metronic.alert({
                                type: 'danger',
                                icon: 'warning',
                                message: tableOptions.dataTable.language.metronicAjaxRequestGeneralError,
                                container: tableWrapper,
                                place: 'prepend'
                            });

                            Metronic.unblockUI(tableContainer);
                        }
                    },

                    "drawCallback": function(oSettings) { // run some code on table redraw
                        if (tableInitialized === false) { // check if table has been initialized
                            tableInitialized = true; // set table initialized
                            table.show(); // display table
                        }
                        Metronic.initUniform($('input[type="checkbox"]', table)); // reinitialize uniform checkboxes on each table reload
                        countSelectedRecords(); // reset selected records indicator

                        // callback for ajax data load
                        if (tableOptions.onDataLoad) {
                            tableOptions.onDataLoad.call(undefined, the);
                        }
                    }
                }
            }, options);

            tableOptions = options;

            // create table's jquery object
            table = $(options.src);
            tableContainer = table.parents(".table-container");

            // apply the special class that used to restyle the default datatable
            var tmp = $.fn.dataTableExt.oStdClasses;

            $.fn.dataTableExt.oStdClasses.sWrapper = $.fn.dataTableExt.oStdClasses.sWrapper + " dataTables_extended_wrapper";
            $.fn.dataTableExt.oStdClasses.sFilterInput = "form-control input-small input-sm input-inline";
            $.fn.dataTableExt.oStdClasses.sLengthSelect = "form-control input-xsmall input-sm input-inline";

            // initialize a datatable
            dataTable = table.DataTable(options.dataTable);

            // revert back to default
            $.fn.dataTableExt.oStdClasses.sWrapper = tmp.sWrapper;
            $.fn.dataTableExt.oStdClasses.sFilterInput = tmp.sFilterInput;
            $.fn.dataTableExt.oStdClasses.sLengthSelect = tmp.sLengthSelect;

            // get table wrapper
            tableWrapper = table.parents('.dataTables_wrapper');
            
            // get custom filter wrapper
            tableCustomFilterWrapper = tableContainer.find('.table-custom-filter-wrapper');

            // get search all wrapper
            tableSearchAllWrapper = tableContainer.find('.table-search-all');

            // build table group actions panel
            if ($('.table-actions-wrapper', tableContainer).size() === 1) {
                $('.table-group-actions', tableWrapper).html($('.table-actions-wrapper', tableContainer).html()); // place the panel inside the wrapper
                $('.table-actions-wrapper', tableContainer).remove(); // remove the template container
            }
            // build table search all panel
            if ($('.table-search-all-wrapper', tableContainer).size() === 1) {
                $('.table-search-all', tableWrapper).html($('.table-search-all-wrapper', tableContainer).html()); // place the panel inside the wrapper
                $('.table-search-all-wrapper', tableContainer).remove(); // remove the template container
            }
            // handle group checkboxes check/uncheck
            $('.group-checkable', table).change(function() {
                var set = $('tbody > tr > td:nth-child(1) input[type="checkbox"]', table);
                var checked = $(this).is(":checked");
                $(set).each(function() {
                    $(this).attr("checked", checked);
                });
                $.uniform.update(set);
                countSelectedRecords();
            });

            // handle row's checkbox click
            table.on('change', 'tbody > tr > td:nth-child(1) input[type="checkbox"]', function() {
                countSelectedRecords();
            });

            // handle filter submit button click
            table.on('click', '.filter-submit', function(e) {
                e.preventDefault();
                the.submitFilter();
            });

            // handle filter cancel button click
            table.on('click', '.filter-cancel', function(e) {
                e.preventDefault();
                the.resetFilter();
            });
            
            tableCustomFilterWrapper.on('click', '.filter-submit', function(e) {
                 e.preventDefault();
                 the.submitFilter();
            });

            tableSearchAllWrapper.on('click', '.filter-cancel', function(e) {
                e.preventDefault();
                the.resetFilter();
            });

            tableSearchAllWrapper.on('click', '.filter-submit', function(e) {
                e.preventDefault();
                the.submitFilter();
            });
        },

        submitFilter: function() {
            the.setAjaxParam("action", tableOptions.filterApplyAction);

            // get all typeable inputs
            /*$('textarea.form-filter, select.form-filter, input.form-filter:not([type="radio"],[type="checkbox"])', table).each(function() {
                the.setAjaxParam($(this).attr("name"), $(this).val());
            });*/
            
            $('.filter td', table).each(function(key, value){
                var $filterElements = $(value).find('textarea.form-filter, select.form-filter, input.form-filter:not([type="radio"],[type="checkbox"])');
                if($filterElements.length > 0) {
                    var filterObject = {regex: false, value: $filterElements.val()}
                    the.setFilterParam(key, filterObject);
                }
            });

            $('.filter td', table).each(function(key, value){
                var $filterElements = $(value).find('input.form-date-from-filter, input.form-date-to-filter');
                if($filterElements.length > 0) {
                    var filterObject = {regex: false, value: $($filterElements.get(0)).val() + ' - ' + $($filterElements.get(1)).val(), datatype: "date"}
                    the.setFilterParam(key, filterObject);
                }
            });

            // get all checkboxes
            /*$('input.form-filter[type="checkbox"]:checked', table).each(function() {
                the.addAjaxParam($(this).attr("name"), $(this).val());
            });*/
            
            $('.filter td', table).each(function(key, value){
                var $filterElements = $(value).find('input.form-filter[type="checkbox"]:checked');
                if($filterElements.length > 0) {
                    var filterObject = {regex: false, value: $filterElements.val()}
                    the.addFilterParam(key, filterObject);
                }
            });

            // get all radio buttons
            /*$('input.form-filter[type="radio"]:checked', table).each(function() {
                the.setAjaxParam($(this).attr("name"), $(this).val());
            });*/
            
            $('.filter td', table).each(function(key, value){
                var $filterElements = $(value).find('input.form-filter[type="radio"]:checked');
                if($filterElements.length > 0) {
                    var filterObject = {regex: false, value: $filterElements.val()}
                    the.setFilterParam(key, filterObject);
                }
            });
            
            //custom filter params
            $('select.custom-filter', tableCustomFilterWrapper).each(function(key, value){
                the.setCustomFilterParam($(value).attr('name'), $(value).val());
            });

            //filter all
            the.setFilterAllParam($('input.search-all-input').val());

            dataTable.ajax.reload();
        },

        resetFilter: function() {
            $('textarea.form-filter, select.form-filter, input.form-filter', table).each(function() {
                $(this).val("");
            });
            $('input.form-filter[type="checkbox"]', table).each(function() {
                $(this).attr("checked", false);
            });
            $('input.search-all-input').val('');
            the.clearAjaxParams();
            the.clearFilterParams();
            the.clearFilterAllParam();
            the.addAjaxParam("action", tableOptions.filterCancelAction);
            dataTable.ajax.reload();
        },

        getSelectedRowsCount: function() {
            return $('tbody > tr > td:nth-child(1) input[type="checkbox"]:checked', table).size();
        },

        getSelectedRows: function() {
            var rows = [];
            $('tbody > tr > td:nth-child(1) input[type="checkbox"]:checked', table).each(function() {
                rows.push($(this).val());
            });

            return rows;
        },

        setAjaxParam: function(name, value) {
            ajaxParams[name] = value;
        },

        addAjaxParam: function(name, value) {
            if (!ajaxParams[name]) {
                ajaxParams[name] = [];
            }

            skip = false;
            for (var i = 0; i < (ajaxParams[name]).length; i++) { // check for duplicates
                if (ajaxParams[name][i] === value) {
                    skip = true;
                }
            }

            if (skip === false) {
                ajaxParams[name].push(value);
            }
        },
        
        setFilterParam: function(column, value) {
            filterParams[column] = value;
        },
        
        addFilterParam: function(column, value) {
            if (!ajaxParams[column]) {
                ajaxParams[column] = [];
            }

            skip = false;
            for (var i = 0; i < (ajaxParams[column]).length; i++) { // check for duplicates
                if (ajaxParams[column][i] === value) {
                    skip = true;
                }
            }

            if (skip === false) {
                ajaxParams[column].push(value);
            }
        },
        
        setCustomFilterParam: function(column, value) {
            customFilterParams[column] = value;
        },

        setFilterAllParam: function(value) {
            filterAllParam = value;
        },
        
        clearAjaxParams: function(name, value) {
            ajaxParams = {};
        },
        
        clearFilterParams: function() {
            filterParams = {};
        },

        clearFilterAllParam: function() {
            filterAllParam = '';
        },

        getDataTable: function() {
            return dataTable;
        },

        getTableWrapper: function() {
            return tableWrapper;
        },

        gettableContainer: function() {
            return tableContainer;
        },

        getTable: function() {
            return table;
        }

    };

};