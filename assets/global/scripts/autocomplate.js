var AUTOCOMPLETE = [];
var AUTOCOMPLETE_INIT_INTERVAL;

$(document).ready(function() {

    AUTOCOMPLETE_INIT_INTERVAL = setInterval(function(){
        if(typeof google != "undefined"){
            clearInterval(AUTOCOMPLETE_INIT_INTERVAL);
            initAutocomplete();
        }
    }, 1000);
});

function initAutocomplete()
{
    $('.autocomplete').each(function(key, value) {

        var element = value;
        var options = {
            types: ['geocode', 'establishment'],
            componentRestrictions: {country: 'us'}
        };

        AUTOCOMPLETE[key] = new google.maps.places.Autocomplete(element, options);

        AUTOCOMPLETE[key].addListener('place_changed', function() {
            changePlace(key, value);
        });

    });
}

function changePlace(key, value)
{
    var componentForm = {
        street_number: {type: "short_name", value: ""},
        route: {type: "short_name", value: ""},
        locality: {type: "long_name", value: ""},
        administrative_area_level_1: {type: "short_name", value: ""},
        country: {type: "short_name", value: ""},
        postal_code: {type: "short_name", value: ""}
    };

    var elementId = $(value).attr('id');
    var place = AUTOCOMPLETE[key].getPlace();

    for (var i = 0; i < place.address_components.length; i++) {

        var componentType = place.address_components[i].types[0];
        if(componentForm.hasOwnProperty(componentType)) {
            componentForm[componentType].value = place.address_components[i][componentForm[componentType].type];
        }
    }
    // var latitude = place.geometry.location.lat();
    // var longitude = place.geometry.location.lng();
    var street = componentForm['street_number'].value == '' ? componentForm['route'].value : componentForm['street_number'].value + " " + componentForm['route'].value;
    // var city = componentForm['locality'].value;
    // var zip = componentForm['postal_code'].value;
    // var state = componentForm['administrative_area_level_1'].value;
    // var country = componentForm['country'].value;

    // $('#'+elementId+'-longitude').val(longitude);
    // $('#'+elementId+'-latitude').val(latitude);
    $('#'+elementId+'-address').val(street);
    // $('#'+elementId+'-city').val(city);
    // $('#'+elementId+'-zip').val(zip);
    // $('#'+elementId+'-state').val(state);
    // $('#'+elementId+'-country').find('option[data-code="'+country+'"]').attr('selected', true);
    // $('#'+elementId+'-country').change();
}
