infoNumber = 0;

$(document).ready(function() {

    infoNumber = $('.info-group-item').length;

    $('.add-video-block').click(function(){

        if (videoNumber != 2) {
            addVideoData($(this));
        }else{
            return false;
        }
    });

    $('#add-info').click(function (e) {
        e.preventDefault();
        addInfoData($(this));
    });

    $(document).on("click", ".remove-info", function (e) {
        e.preventDefault();
        $(this).closest('.info-group-item').remove();

    });

    $(document).on('click', '.removeImageGallery',function (e) {
        e.preventDefault();
        removeGaleryImage($(this));
    });
    $(document).on('click', '.removeImagePartners',function (e) {
        e.preventDefault();
        removePartnerImage($(this));
    });
    $(document).on('click', '.removeImageService',function (e) {
        e.preventDefault();
        removeServicesImage($(this));
    });
    $(document).on('click', '.removeImageTeam',function (e) {
        e.preventDefault();
        removeTeamImage($(this));
    });


});


function initEditor()
{
    if(jQuery().wysihtml5) {

        $('.wysihtml5:not(.new-added)').wysihtml5({
            "stylesheets": ["/assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
        });
    }
}

function addInfoData(filename)
{

    var $clonedObject = $('#template-info').clone();
    var html = $clonedObject.html();

    html = html.replace(/{{filename}}/g, filename);
    html = html.replace(/{{number}}/g, infoNumber);
    $('#info-data').append(html);

    infoNumber++;
}

function removeGaleryImage($this) {
    var filename = $this.data('filename');
    $.ajax({
        url: '/service/removeGaleryImage',
        method:'post',
        dataType:"json",
        data:{filename:filename},
        success: function(response, status, xhr, $form) {
            if (response.success)
                $this.closest('.image-lenght').fadeOut(function(){
                    $(this).remove();
                });
        }
    })
}
function removePartnerImage($this) {
    var filename = $this.data('filename');
    $.ajax({
        url: '/service/removePartnersImage',
        method:'post',
        dataType:"json",
        data:{filename:filename},
        success: function(response, status, xhr, $form) {
            if (response.success)
            $this.closest('.image-lenght').fadeOut(function(){
                $(this).remove();
            });
        }
    })
}

function removeServicesImage($this) {

    var filename = $this.data('filename');
    $.ajax({
        url: '/service/removeServicesImage',
        method:'post',
        dataType:"json",
        data:{filename:filename},
        success: function(response, status, xhr, $form) {
            if (response.success)
                $('#userfile').prop('disabled', false);
                $this.closest('.image-lenght').fadeOut(function(){
                    $(this).remove();
                });
        }
    })
}

function removeTeamImage($this) {

    var filename = $this.data('filename');
    $.ajax({
        url: '/service/removeTeamImage',
        method:'post',
        dataType:"json",
        data:{filename:filename},
        success: function(response, status, xhr, $form) {
            if (response.success)
                $('#userfile').prop('disabled', false);
            $this.closest('.image-lenght').fadeOut(function(){
                $(this).remove();
            });
        }
    })
}