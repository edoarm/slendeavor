$(document).ready(function() {
initValidation();

})


function initValidation()
{
    $.validator.setDefaults({
        onkeyup: false,
        //errorElement: 'span',
        errorClass: 'help-block help-block-error',
        focusInvalid: false,
        errorPlacement: function (error, element) {
            var icon = $(element).parent('.input-icon-feedback').children('i');
            icon.removeClass('fa-check').addClass("fa-warning"); 
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass("has-success").addClass('has-error');  
        },
        success: function (label, element) {
            var icon = $(element).parent('.input-icon-feedback').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
        }

    });

    $('#signup-form').validate({
          rules: {
              firstname: "required",
              lastname: "required",
              email: {
                  required: true,
                  email: true
              },
              pass: {
                  required: true,
                  minlength: 6
              },
              cpass: {
                  required: true,
                  equalTo: "#password"
              },
              phone: "required",
              type: "required"
          },
          submitHandler: function(form) {
            form.submit();  
          }
      });
}