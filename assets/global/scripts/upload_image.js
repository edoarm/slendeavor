
var count = null;

$(document).ready(function() {

    count = $('.image-lenght').length;

    $("#userfile").on("change", function() {
        window.tr = '';
        var inp = document.getElementById('userfile');
        $('#userfilenames').html(function(index) {
            var returnString = "";

            for (var i = 0; i < inp.files.length; ++i) {
                var name = inp.files.item(i).name;

                readImage( inp.files[i],"image"+i );

                returnString +='<div class="col-lg-3 col-sm-12 image-lenght">'+
                    '<div class="image-content">'+
                    '<div class="button-remove">'+
                    '<a href="#" class="imgDelete" ><i class="fa fa-remove"></i></a>'+
                    '</div>'+
                    '<div class="form-group  m-form__group">'+
                    '<input type="text" name="priority['+count+']" class="form-control m-input" value="'+parseInt(i+1)+'" placeholder="Priority">'+
                    '</div>'+
                    '<div class="image-wrapp" id="image'+i+'">'+
                    '</div>'+
                    '<div class="form-group  m-form__group">'+
                    '<input type="text" name="new_title['+count+']" class="form-control m-input" value="'+name+'" placeholder="Name Alt">'+
                    '</div>'+
                    '<input type="hidden" name="image_id['+count+']" value="0">'+
                    '</div>'+
                    '</div>';

                count++;
            }
            return returnString;
        });

    });
    $(document).on('click', '.imgDelete',function (e) {
        e.preventDefault();
        $(this).closest('.image-lenght').fadeOut(function(){
            $(this).remove();
        });
    });

});



function readImage(file,id) {
    var reader = new FileReader();
    var image  = new Image();
    reader.readAsDataURL(file);
    reader.onload = function(_file) {
        image.src    = _file.target.result;              // url.createObjectURL(file);
        image.onload = function() {
            var w = this.width,
                h = this.height,
                t = file.type,                           // ext only: // file.type.split('/')[1],
                n = file.name,
                s = ~~(file.size/1024) +'KB';
            $('#'+id).append('<img src="'+ this.src +'">');
        };
        image.onerror= function() {
            alert('Invalid file type: '+ file.type);
        };
    };
}
// $('#images').sortable({
//     stop: function( event, ui ) {
//         var sort_url = $(this).attr('data-url');
//         var json = $( "#images" ).sortable( "toArray" );
//         $.ajax({
//             type: 'POST',
//             url: sort_url,
//             data: {images: json}
//         })
//     }
// });
